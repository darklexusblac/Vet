<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

trait Catalog
{

    /**
     * @return Model
     */
    public static function getModel()
    {
        return static::$model;
    }

    /**
     * @return string
     */
    public static function getOrderField(): string
    {
        return static::$order_field ?? 'name';
    }

    public static function getPerPage(): string
    {
        return static::$per_page ?? 'perPage';
    }

    public static function getResourceNamespace(): string
    {
        return static::$resource_namespace ?? 'App\Http\Resources\\';
    }

    public static function getResourceSuffix(): string
    {
        return static::$resource_suffix ?? 'Resource';
    }

    public static function getDisk(): string
    {
        return static::$disk ?? 'public';
    }

    /**
     * @param $request
     * @param bool $need_resource
     * @param null|JsonResource|string $resource_name
     * @return mixed
     */
    public function indexCatalog($request, $condition = false, $need_resource = false, $resource_name = null)
    {
        $result = static::getModel()::when(!empty($condition) && is_callable($condition), $condition)
            ->orderBy(static::getOrderField())
            ->paginate($request->get(static::getPerPage()));

        if ($need_resource) {
            if (empty($resource_name)) {
                $resource_name = (static::getResourceNamespace() . class_basename(static::getModel()) . static::getResourceSuffix());
            }
            $result = $resource_name::collection($result)->response()->getData(true);
        }

        return $result;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function saveCatalog($request)
    {
        $result = $this->saveCatalogRaw($request);

        return $this->success(__('Успешно ' . $result['message']), [
            'items' => $this->index($request)
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function saveCatalogRaw($request): array
    {
        if (empty($request->get('id'))) {
            $model = static::getModel();
            $model = new $model();
            $message = 'добавлено';
        } else {
            $model = static::getModel()::find($request->get('id'));
            $message = 'обновлено';
        }

        if (property_exists($model, 'field_files')) {
            $data = $request->except($model->field_files);
        }
        else {
            $data = $request->all();
        }

        $model->fill($data);
        $model->save();

        if (!empty($request->allFiles())) {
            $field_files = [];
            foreach ($model->field_files as $file) {

                $request_file = $request->file($file);
                if (!$request->hasFile($file)) {
                    continue;
                }

                $path = Storage::disk(static::getDisk())->putFileAs(
                    $model->id, $request_file, $file . '.' . $request_file->extension()
                );
                $field_files[$file] = $path;

            }

            if (!empty($field_files)) {
                $model->update($field_files);
            }
        }

        return [
            'message' => $message,
            'model' => $model
        ];
    }

    public function deleteCatalog(Request $request)
    {

        static::getModel()::destroy($request->get('ids'));

        return $this->success(__('Успешно удалено'), [
            'items' => $this->index($request)
        ]);
    }

    public function all()
    {
        return $this->success('', ['items' => static::getModel()::all()]);
    }

}
