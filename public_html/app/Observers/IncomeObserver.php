<?php

namespace App\Observers;

use App\Models\Income;

class IncomeObserver
{
    /**
     * Handle the Income "created" event.
     *
     * @param  \App\Models\Income  $income
     * @return void
     */
    public function created(Income $income)
    {
//        $product = $income->product;
//        $product->update([
//            'price_purchase' => $income->price_purchase,
//            'price_retail' => $income->price_retail,
//            'price_opt' => $income->price_opt,
//            'price_internet' => $income->price_internet,
//            'price_partner' => $income->price_partner,
//            'currency_id' => $income->currency_id,
//            'currency_purchase_id' => $income->currency_purchase_id,
//        ]);
    }

    /**
     * Handle the Income "updated" event.
     *
     * @param  \App\Models\Income  $income
     * @return void
     */
    public function updated(Income $income)
    {
        //
    }

    /**
     * Handle the Income "deleted" event.
     *
     * @param  \App\Models\Income  $income
     * @return void
     */
    public function deleted(Income $income)
    {
        //
    }

    /**
     * Handle the Income "restored" event.
     *
     * @param  \App\Models\Income  $income
     * @return void
     */
    public function restored(Income $income)
    {
        //
    }

    /**
     * Handle the Income "force deleted" event.
     *
     * @param  \App\Models\Income  $income
     * @return void
     */
    public function forceDeleted(Income $income)
    {
        //
    }
}
