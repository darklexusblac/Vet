<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'photo',
        'contact',
        'phone',
        'address',
        'email'
    ];

    const DISK = 'vendor';
    public $field_files = ['photo'];
}
