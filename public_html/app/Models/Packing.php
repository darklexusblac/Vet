<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Packing extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'photo'
    ];

    const DISK = 'packing';
    public $field_files = ['photo'];
}
