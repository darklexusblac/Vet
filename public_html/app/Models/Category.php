<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
    use HasFactory, NodeTrait;

    protected $fillable = [
        'name',
        'description',
        'photo',
        'parent_id'
    ];

    const DISK = 'category';
    public $field_files = ['photo'];

    public function warehouse()
    {
        $warehouse_id = request()->get('warehouse_id', false);
        return $this->belongsToMany(Warehouse::class)->when($warehouse_id, function ($q) use ($warehouse_id) {
            $q->where('id', '=', $warehouse_id);
        });
    }
}
