<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Builder;

use App\Modules\Vet;

class Product extends Model
{
    use HasFactory;

    const DISK = 'products';
    const SEARCH_MIN_COUNT_WORD = 3;



    protected $fillable = [
        'name',
        'description',
        'code',
        'price_purchase',
        'price_retail',
        'price_opt',
        'price_internet',
        'price_partner',
        'photo',
        'guarantee',
        'minimum_balance',
        'is_service',
        'qty_in_package',
        'weight',
        'length',
        'width',
        'height',
        'is_not_in_price',
        'manufacturer_barcode',
        'internal_barcode_1',
        'internal_barcode_2',
        'internal_barcode_3',
        'internal_barcode_4',
        'excise_barcode',
        'is_ignore_discount',
        'is_excise',
        'is_ignore_extra_change',
        'is_import',
        'category_id',
        'currency_id',
        'currency_purchase_id',
        'nds_id',
        'packing_id',
        'set_id',
        'unit_id',
        'vendor_id',
        'user_id',
        'user_change_id',
        'custom_fields',
        'analog_ids'
    ];
    protected $casts = [
        'service' => 'boolean',
        'is_not_in_price' => 'boolean',
        'is_ignore_discount' => 'boolean',
        'is_excise' => 'boolean',
        'is_ignore_extra_change' => 'boolean',
        'is_import' => 'boolean'
    ];

    public $field_files = ['photo'];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return BelongsTo
     */
    public function currency_purchase(): BelongsTo
    {
        return $this->belongsTo(Currency::class, 'currency_purchase_id');
    }

    /**
     * @return BelongsTo
     */
    public function nds(): BelongsTo
    {
        return $this->belongsTo(Nds::class);
    }

    /**
     * @return BelongsTo
     */
    public function packing(): BelongsTo
    {
        return $this->belongsTo(Packing::class);
    }

    /**
     * @return BelongsTo
     */
    public function set(): BelongsTo
    {
        return $this->belongsTo(Set::class);
    }

    /**
     * @return BelongsTo
     */
    public function unit(): BelongsTo
    {
        return $this->belongsTo(Unit::class);
    }

    /**
     * @return BelongsTo
     */
    public function vendor(): BelongsTo
    {
        return $this->belongsTo(Vendor::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function user_change(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_change_id');
    }

    public function prices(): HasMany
    {
        return $this->hasMany(Price::class);
    }

    public function getCustomFieldsAttribute(): array
    {
        $result = $this->attributes['custom_fields'];
        if (empty($result)) {
            $result = [];
        }
        elseif (Vet::isJson($result)) {
            $result = json_decode($result, true);
        }

        return $result;
    }

    public function getAnalogIdsAttribute(): array
    {
        $result = $this->attributes['analog_ids'];
        if (empty($result)) {
            $result = [];
        }
        elseif (Vet::isJson($result)) {
            $result = json_decode($result, true);
        }

        return $result;
    }

    public function incomes()
    {
        return $this->belongsToMany(Income::class);
    }

//    public function warehouses()
//    {
//        return $this->belongsToMany(Warehouse::class);
//    }


    public function scopeQuerySearch(Builder $query, string $word): Builder
    {
        return $query->where(function ($q) use ($word) {
            $q->where('name', 'like', '%' . $word . '%')
                ->orWhere('manufacturer_barcode', 'like', $word . '%')
                ->orWhere('internal_barcode_1', 'like', $word . '%')
                ->orWhere('internal_barcode_2', 'like', $word . '%')
                ->orWhere('internal_barcode_3', 'like', $word . '%')
                ->orWhere('internal_barcode_4', 'like', $word . '%')
                ->orWhere('excise_barcode', 'like', $word . '%');
        });
    }
}
