<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Warehouse extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'phone',
        'contact',
        'address'
    ];

    const DISK = 'warehouse';
    public $field_files = [];

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }
}
