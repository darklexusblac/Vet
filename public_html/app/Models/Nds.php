<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nds extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'percent',
        'description'
    ];

    const DISK = 'nds';
    public $field_files = [];
}
