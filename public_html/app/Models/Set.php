<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'photo'
    ];

    const DISK = 'set';
    public $field_files = ['photo'];
}
