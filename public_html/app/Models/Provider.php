<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'contact',
        'phone',
        'email',
        'address',
    ];

    const DISK = 'Provider';
    public $field_files = [];

//    public function products()
//    {
//        return $this->morphToMany(Product::class,'productable')->withTimestamps();
//    }
}
