<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Markup extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'percent1',
        'percent2',
        'percent3',
        'percent4',
        'percent5'
    ];

    const DISK = 'markup';
    public $field_files = [];
}
