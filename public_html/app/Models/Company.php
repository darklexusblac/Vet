<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Company extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'phone',
        'address',
        'email',
        'www',
        'director',
        'booker',
        'account',
        'mfo',
        'bank',
        'photo'
    ];
    const DISK = 'company';
    public $field_files = ['photo'];
}
