<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class CreateLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'links:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Links with folders';

    protected $disks = [
        'products'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach ($this->disks as $disk_name) {
            Storage::disk($disk_name)->makeDirectory('');
        }

        Artisan::call('storage:link');
        return 0;
    }
}
