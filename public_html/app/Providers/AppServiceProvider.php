<?php

namespace App\Providers;

use App\Models\Income;
use App\Observers\IncomeObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'provider' => 'App\Models\Provider',
            'product' => 'App\Models\Product',
            'warehouse' => 'App\Models\Warehouse',
        ]);

        Income::observe(IncomeObserver::class);
    }
}
