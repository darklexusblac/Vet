<?php

namespace App\Http\Requests;

use App\Models\Provider;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProviderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $model = Provider::class;
        $id = '';
        if ($this->get('id', false)) {
            $item = $model::find($this->get('id'));
            $id = $item->id;
        }
        $rules = [
            'name' => ['required', Rule::unique((new $model)->getTable())->ignore($id)],
//            'photo'=> $this->hasFile('photo') ? 'image' : '',
            'contact' => 'nullable',
            'phone' => 'nullable|numeric',
            'email' => 'nullable|email',
            'address' => 'nullable'

        ];

        if ($this->has('ids')) {
            $rules = [
                'ids' => 'required'
            ];
        }

        return $rules;
    }
}
