<?php

namespace App\Http\Requests;

use App\Models\Markup;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MarkupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $model = Markup::class;
        $id = '';
        if ($this->get('id', false)) {
            $item = $model::find($this->get('id'));
            $id = $item->id;
        }
        $rules = [
            'name' => ['required', Rule::unique((new $model)->getTable())->ignore($id)],
//            'photo'=> $this->hasFile('photo') ? 'image' : '',
            'description' => 'nullable',
            'percent1' => 'nullable|numeric',
            'percent2' => 'nullable|numeric',
            'percent3' => 'nullable|numeric',
            'percent4' => 'nullable|numeric',
            'percent5' => 'nullable|numeric'

        ];

        if ($this->has('ids')) {
            $rules = [
                'ids' => 'required'
            ];
        }

        return $rules;
    }
}
