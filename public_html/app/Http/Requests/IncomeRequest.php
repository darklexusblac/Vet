<?php

namespace App\Http\Requests;

use App\Models\Income;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IncomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $model = Income::class;
        $id = '';
        if ($this->get('id', false)) {
            $item = $model::find($this->get('id'));
            $id = $item->id;
        }

        $rules = [
            'warehouse_id' => 'required',
            'provider_id' => 'required',
            'invoice_number' => 'required',
            'product_count' => 'nullable'
        ];

        if ($this->has('ids')) {
            $rules = [
                'ids' => 'required'
            ];
        }

        return $rules;
    }
}
