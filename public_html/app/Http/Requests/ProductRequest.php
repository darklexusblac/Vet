<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $model = Product::class;

        foreach ($this->all() as $key => $item) {
            /*if () {
                $res = json_decode($item, 1);
                $this->merge([$key => $res]);
            }*/
        }

        $id = '';
        if ($this->get('id', false)) {
            $item = $model::find($this->get('id'));
            $id = $item->id;
        }
        $rules = [
            'name' => ['required', Rule::unique((new $model)->getTable())->ignore($id)],
            'description' => '',
            'code' => '',
            'price_purchase' => '',
            'price_retail' => '',
            'price_opt' => '',
            'photo' => '',
            'count' => '',
            'guarantee' => '',
            'minimum_balance' => '',
            'is_service' => '',
            'qty_in_package' => '',
            'weight' => '',
            'length' => '',
            'width' => '',
            'height' => '',
            'is_not_in_price' => '',
            'manufacturer_barcode' => ['nullable', Rule::unique((new $model)->getTable())->ignore($id)],
            'internal_barcode_1' => ['nullable', Rule::unique((new $model)->getTable())->ignore($id)],
            'internal_barcode_2' => ['nullable', Rule::unique((new $model)->getTable())->ignore($id)],
            'internal_barcode_3' => ['nullable', Rule::unique((new $model)->getTable())->ignore($id)],
            'internal_barcode_4' => ['nullable', Rule::unique((new $model)->getTable())->ignore($id)],
            'excise_barcode' => ['nullable', Rule::unique((new $model)->getTable())->ignore($id)],
            'is_ignore_discount' => '',
            'is_excise' => '',
            'is_ignore_extra_change' => '',
            'is_import' => '',
            'category_id' => '',
            'company_id' => '',
            'currency_id' => '',
            'currency_purchase_id' => '',
            'nds_id' => '',
            'packing_id' => '',
            'set_id' => '',
            'unit_id' => '',
            'vendor_id' => '',
            'warehouse_id' => '',
            'user_id' => '',
            'user_change_id' => '',
            'custom_fields' => '',
            'analog_ids' => ''
        ];

        if ($this->has('ids')) {
            $rules = [
                'ids' => 'required'
            ];
        }

        return $rules;
    }
}
