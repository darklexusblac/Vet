<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $results = $this->resource->toArray();

        foreach ($this->resource->field_files as $field) {
            if (empty($this->{$field})) {
                $results[$field] = '';
            } else {
                $results[$field] = Storage::disk($this->resource::DISK)->url($this->{$field});
            }
        }

        return $results;
    }
}
