<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class IncomeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $results = $this->resource->toArray();

        $results['warehouse'] = $this->warehouse;
        $results['products'] = $this->products;
        $results['provider'] = $this->provider;
//        $results['product_count'] = $this->getProductsCount();


        foreach ($this->resource->field_files as $field) {
            if (empty($this->{$field})) {
                $results[$field] = '';
            } else {
                $results[$field] = Storage::disk($this->resource::DISK)->url($this->{$field});
            }
        }

        return $results;
    }

    public function getProductsCount(): array
    {
        $result = [];
        foreach ($this->products as $product) {
            $result[] = [
                'product_id' => $product->id,
                'count' => $product->pivot->count,
                'name' => $product->name
            ];
        }

        return $result;
    }
}
