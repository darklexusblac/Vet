<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Packing;
use App\Traits\Catalog;
use Illuminate\Http\Request;
use App\Http\Requests\PackingRequest;

class PackingController extends Controller
{
    use Catalog;

    protected static $model = Packing::class;

    protected static $disk = Packing::DISK;

    public function index(Request $request)
    {
//        var_dump($request->all());exit;
        return $this->indexCatalog($request, false,true);
    }

    public function save(PackingRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(PackingRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
