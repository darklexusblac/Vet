<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Sale;
use App\Traits\Catalog;
use Illuminate\Http\Request;
use App\Http\Requests\SaleRequest;

class SaleController extends Controller
{
    use Catalog;

    protected static $model = Sale::class;

    protected static $disk = Sale::DISK;

    public function index(Request $request)
    {
        return $this->indexCatalog($request, false,true);
    }

    public function save(SaleRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(SaleRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
