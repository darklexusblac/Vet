<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use App\Traits\Catalog;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    use Catalog;

    protected static $model = Company::class;

    protected static $disk = Company::DISK;

    public function index(Request $request)
    {
        return $this->indexCatalog($request, false, true);
    }

    public function save(CompanyRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(CompanyRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
