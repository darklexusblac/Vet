<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\WarehouseRequest;
use App\Models\Category;
use App\Models\Warehouse;
use App\Modules\Vet;
use App\Traits\Catalog;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    use Catalog;

    protected static $model = Warehouse::class;

    protected static $disk = Warehouse::DISK;

    public function index(Request $request)
    {
        return $this->indexCatalog($request, false, true);
    }

    public function save(WarehouseRequest $request)
    {

        $result = $this->saveCatalogRaw($request);
        $model = $result['model'];

        $categories = $request->get('categories');

        if (Vet::isJson($categories)) {
            $categories = json_decode($categories, true);
        }

        $categories = Category::whereIn('id', $categories)->get();

        $cids = [];
        foreach ($categories as $item) {
            $cids[] = $item->id;

            if (!$model->categories->contains($item)) {
                $model->categories()->attach($item);
            }
        }

        $model->refresh();
        $ncids = $model->categories->pluck('id')->values()->all();

        $diffs_arrays = array_diff($ncids, $cids);
        if (!empty($diffs_arrays)) {
            $model->categories()->detach($diffs_arrays);
        }

        return $this->success(__('Успешно ' . $result['message']), [
            'items' => $this->index($request)
        ]);

    }

    public function delete(WarehouseRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
