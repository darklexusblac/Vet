<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SetRequest;
use App\Models\Set;
use App\Traits\Catalog;
use Illuminate\Http\Request;

class SetController extends Controller
{
    use Catalog;

    protected static $model = Set::class;

    protected static $disk = Set::DISK;

    public function index(Request $request)
    {
//        var_dump($request->all());exit;
        return $this->indexCatalog($request, false,true);
    }

    public function save(SetRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(SetRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
