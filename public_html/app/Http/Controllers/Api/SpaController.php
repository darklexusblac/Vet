<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Sanctum\NewAccessToken;


class SpaController extends Controller
{

    /**
     * Авторизация
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function token(Request $request): JsonResponse
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'device_name' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        return response()->json([
            'token' => $user->createToken($request->device_name)->plainTextToken
        ]);
    }

    /**
     * Выход
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function rm_token()
    {
        request()->user()->tokens()->delete();

        // Удаление конкретного токена пользователя
        // Get user who requested the logout
        //$user = request()->user(); //or Auth::user()
        // Revoke current user token
        //$user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
    }

    public function user(Request $request)
    {

        return $this->success('', ['user' => auth()->user()]);
    }
}
