<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Markup;
use App\Traits\Catalog;
use Illuminate\Http\Request;
use App\Http\Requests\MarkupRequest;

class MarkupController extends Controller
{
    use Catalog;

    protected static $model = Markup::class;

    protected static $disk = Markup::DISK;

    public function index(Request $request)
    {
//        var_dump($request->all());exit;
        return $this->indexCatalog($request, false,true);
    }

    public function save(MarkupRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(MarkupRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
