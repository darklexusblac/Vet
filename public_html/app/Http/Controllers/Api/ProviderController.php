<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProviderRequest;
use App\Models\Provider;
use App\Traits\Catalog;
use Illuminate\Http\Request;

class ProviderController extends Controller
{
    use Catalog;

    protected static $model = Provider::class;

    protected static $disk = Provider::DISK;

    public function index(Request $request)
    {
        return $this->indexCatalog($request, false,true);
    }

    public function save(ProviderRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(ProviderRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
