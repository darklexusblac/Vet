<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Traits\Catalog;
use Illuminate\Http\Request;
use App\Http\Requests\CurrencyRequest;

class CurrencyController extends Controller
{
    use Catalog;

    protected static $model = Currency::class;

    protected static $disk = Currency::DISK;

    public function index(Request $request)
    {
        return $this->indexCatalog($request, false, true);
    }

    public function save(CurrencyRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(CurrencyRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
