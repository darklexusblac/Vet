<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Modules\Vet;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\ProductRequest;
use App\Traits\Catalog;

class ProductController extends Controller
{
    use Catalog;

    protected static $model = Product::class;

    protected static $disk = Product::DISK;

    public function index(Request $request)
    {
        $filters = $request->get('filters');


        if (empty($filters)) {

            $filters = '[]';
        }


        if (Vet::isJson($filters)) {
            $filters = json_decode($filters, true);
        }

        $queryString = '';
        foreach ($filters as $filter) {
            if (isset($filter['warehouse_id'])) {
                $request->request->set('warehouse_id', $filter['warehouse_id']);
            }
            if (isset($filter['query'])) {
                $queryString = $filter['query'];
            }
        }

        $condition = function (Builder $query) use ($queryString) {
            $query
//                ->whereHas('category.warehouse')
                ->with('category.warehouse')
                ->when(!empty($queryString), function ($q) use ($queryString) {
                    $q->querySearch($queryString);
                });
        };

        return $this->indexCatalog($request, $condition, true);

    }

    public function save(ProductRequest $request)
    {
        if (!empty($request->get('id'))) {
            $user = 'user_change_id';
        } else {
            $user = 'user_id';
        }

        $request->merge([
            $user => auth()->user()->id
        ]);

        return $this->saveCatalog($request);
    }

    public function delete(ProductRequest $request)
    {
        return $this->deleteCatalog($request);
    }

    public function search(Request $request)
    {
        $query = $request->get('query', '');
        $query_perPage = $request->get('perPage', '');
        $is_current_len = strlen(str_replace(' ', '', $query)) >= Product::SEARCH_MIN_COUNT_WORD;


        $result = [];

        if ($is_current_len && $request->get('warehouse_id', false)) {

            $result = Product::when(!empty($query), function ($q) use ($query) {
                $q->querySearch($query);
            })
//                ->with('category.warehouse')
                ->whereHas('category.warehouse')
                ->when($query_perPage, function ($q) use ($query_perPage) {
                    $q->limit($query_perPage);
                })
                ->get();
        }

        return $this->success('', [
            'items' => $result
        ]);
    }

}
