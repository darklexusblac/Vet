<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NdsRequest;
use App\Models\Nds;
use App\Traits\Catalog;
use Illuminate\Http\Request;

class NdsController extends Controller
{
    use Catalog;

    protected static $model = Nds::class;

    protected static $disk = Nds::DISK;

    public function index(Request $request)
    {
//        var_dump($request->all());exit;
        return $this->indexCatalog($request, false,true);
    }

    public function save(NdsRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(NdsRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
