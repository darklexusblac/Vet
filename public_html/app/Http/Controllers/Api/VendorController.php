<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VendorRequest;
use App\Models\Vendor;
use App\Traits\Catalog;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    use Catalog;

    protected static $model = Vendor::class;

    protected static $disk = Vendor::DISK;

    public function index(Request $request)
    {
//        var_dump($request->all());exit;
        return $this->indexCatalog($request, false, true);
    }

    public function save(VendorRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(VendorRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
