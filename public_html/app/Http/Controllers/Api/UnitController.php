<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UnitRequest;
use App\Models\Unit;
use App\Traits\Catalog;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    use Catalog;

    protected static $model = Unit::class;

    protected static $disk = Unit::DISK;

    public function index(Request $request)
    {
//        var_dump($request->all());exit;
        return $this->indexCatalog($request, false, true);
    }

    public function save(UnitRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(UnitRequest $request)
    {
        return $this->deleteCatalog($request);
    }
}
