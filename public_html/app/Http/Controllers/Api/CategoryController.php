<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Traits\Catalog;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use Catalog;

    protected static $model = Category::class;

    protected static $disk = Category::DISK;

    public function index(Request $request)
    {
        return $this->indexCatalog($request,false, true);
    }

    public function save(CategoryRequest $request)
    {
        return $this->saveCatalog($request);
    }

    public function delete(CategoryRequest $request)
    {
        return $this->deleteCatalog($request);
    }

    public function all()
    {
        return $this->success('', ['items' => static::getModel()::get()->toTree()]);
    }

    public function getChildren ($root)
    {
        return $this->success('', [
            'items' => static::getModel()::get()->toFlatTree($root)->pluck('id')->values()->all()
        ]);
    }

    public function allWithoutTree()
    {
        return $this->success('', ['items' => static::getModel()::get()->toFlatTree()]);
    }
}
