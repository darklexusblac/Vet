<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\IncomeRequest;
use App\Models\Income;
use App\Models\Product;
use App\Modules\Vet;
use App\Traits\Catalog;
use Illuminate\Http\Request;

class IncomeController extends Controller
{
    use Catalog;

    protected static $model = Income::class;

    protected static $disk = Income::DISK;

    public function index(Request $request)
    {
        return $this->indexCatalog($request, false,true);
    }

    public function save(IncomeRequest $request)
    {
        $result = $this->saveCatalogRaw($request);
        $model = $result['model'];

        $product_count = $request->get('product_count');
        if (Vet::isJson($product_count)) {
            $product_count = json_decode($product_count, true);
        }

        $products = Product::whereIn('id', collect($product_count)->pluck('product_id')->values()->all())->get();

        $pids = [];
        foreach ($product_count as $item) {


            $pids[] = $item['product_id'];
            $product = $products->firstWhere('id', '=', $item['product_id']);

            if ($model->products->contains($product)) {
                $model->products()->updateExistingPivot($product->id, ['count' => $item['count']]);
            } else {
                $model->products()->attach($product, ['count' => $item['count']]);
            }

        }

        $model->refresh();
        $npids = $model->products->pluck('id')->values()->all();

        $diffs_arrays = array_diff($npids, $pids);

        foreach ($diffs_arrays as $diff) {
            $model->products()->detach([$diff]);
        }

        return $this->success(__('Успешно ' . $result['message']), [
            'items' => $this->index($request)
        ]);
    }

    public function delete(IncomeRequest $request)
    {

        $incomes = Income::whereIn('id', $request->get('ids', []))->get();

        foreach ($incomes as $item) {
            /** @var Income $item */
            $products = $item->products;

            foreach ($products as $product) {
                $item->products()->detach($product);
            }

        }

        return $this->deleteCatalog($request);
    }
}
