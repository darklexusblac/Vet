<?php

namespace Database\Seeders;

use App\Models\Warehouse;
use Illuminate\Database\Seeder;

class WarehouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $warehouses = [
            [
                'name' => 'Склад 1'
            ],
            [
                'name' => 'Склад 2'
            ]
        ];

        foreach ($warehouses as $warehouse)
        {
            Warehouse::create($warehouse);
        }
    }
}
