<?php

namespace Database\Seeders;

use App\Models\Markup;
use Illuminate\Database\Seeder;

class MarkupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $markups = [
            [
                'name' => 'Standart',
                'percent1' => '30',
                'percent2' => '25',
                'percent3' => '20',
                'percent4' => '15',
                'percent5' => '10'
            ],
        ];

        foreach ($markups as $markup)
        {
            Markup::create($markup);
        }
    }
}
