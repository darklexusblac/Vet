<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array[] = [
            "name" => "Антибиотики",
            "description" => 'Антибиотики для животных',
            'children' => [
                [
                    "name" => "Кошки",
                    "description" => 'Антибиотики для кошек',
                ],
                [
                    "name" => "Собаки",
                    "description" => 'Антибиотики для собак',
                ],
                [
                    "name" => "Попугаи",
                    "description" => 'Антибиотики для попугаев',
                ],
                [
                    "name" => "Грызуны",
                    "description" => 'Антибиотики для грызунов',
                    'children' => [
                        [
                            "name" => "Крысы",
                            "description" => 'Антибиотики для крыс',
                            'children' => [
                                [
                                    "name" => "Шиншилы",
                                    "description" => 'Антибиотики для шиншил',
                                ],
                                [
                                    "name" => "Хомячки",
                                    "description" => 'Антибиотики для хомячков',
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        foreach ($array as $item) {
            Category::create($item);
        }
    }

}
