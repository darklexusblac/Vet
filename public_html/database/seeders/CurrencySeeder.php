<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currency = [
            [
                'name' => 'USD',
                'description' => 'USD',
                'rate' => '27'
            ],
            [
                'name' => 'UAH',
                'description' => 'UAH',
                'rate' => '1'
            ]
        ];

        foreach ($currency as $item) {
            Currency::create($item);
        }
    }
}
