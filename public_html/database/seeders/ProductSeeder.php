<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name' => 'Test',
                'manufacturer_barcode' => '111',
                'category_id' => '1'
            ],
            [
                'name' => 'Test 1',
                'manufacturer_barcode' => '222',
                'category_id' => '1'
            ]
        ];

        foreach ($products as $product) {
            Product::create($product);
        }
    }
}
