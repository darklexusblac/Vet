<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->integer('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('email')->nullable();
            $table->string('www')->nullable();
            $table->string('director')->nullable();
            $table->string('booker')->nullable();
            $table->string('account')->nullable();
            $table->string('mfo')->nullable();
            $table->string('bank')->nullable();
            $table->string('photo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
