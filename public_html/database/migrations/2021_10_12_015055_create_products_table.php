<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->string('code')->nullable();
            $table->decimal('price_purchase', 10, 2)->nullable();
            $table->decimal('price_retail', 10, 2)->nullable();
            $table->decimal('price_opt', 10, 2)->nullable();
            $table->decimal('price_internet', 10, 2)->nullable();
            $table->decimal('price_partner', 10, 2)->nullable();
            $table->string('photo')->nullable();
            $table->integer('guarantee')->nullable();
            $table->integer('minimum_balance')->default(0);
            $table->tinyInteger('service')->default(0);
            $table->integer('qty_in_package')->nullable();
            $table->decimal('weight',10,2)->nullable();
            $table->decimal('length',10,2)->nullable();
            $table->decimal('width',10, 2)->nullable();
            $table->decimal('height',10,2)->nullable();
            $table->tinyInteger('is_not_in_price')->default(0);
            $table->string('manufacturer_barcode')->nullable();
            $table->string('internal_barcode_1')->nullable();
            $table->string('internal_barcode_2')->nullable();
            $table->string('internal_barcode_3')->nullable();
            $table->string('internal_barcode_4')->nullable();
            $table->string('excise_barcode')->nullable();
            $table->tinyInteger('is_ignore_discount')->default(0);
            $table->tinyInteger('is_excise')->default(0);
            $table->tinyInteger('is_ignore_extra_change')->default(0);
            $table->tinyInteger('is_import')->default(0);
            $table->integer('category_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('currency_id')->nullable();
            $table->integer('currency_purchase_id')->nullable();
            $table->integer('nds_id')->nullable();
            $table->integer('packing_id')->nullable();
            $table->integer('set_id')->nullable();
            $table->integer('unit_id')->nullable();
            $table->integer('vendor_id')->nullable();
//            $table->integer('warehouse_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('user_change_id')->nullable();
            $table->json('custom_fields')->nullable();
            $table->json('analog_ids')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
