import Vue from 'vue';
import Router from 'vue-router';

// Компоненты
import NotFound from '../components/NotFound';
import Base from '../components/Base';
import Dashboard from '../components/Dashboard';
import Product from '../components/Product';
import Currency from '../components/Currency';
import Category from '../components/Category';
import Company from '../components/Company';
import Nds from '../components/Nds';
import Markup from '../components/Markup';
import Packing from '../components/Packing';
import Set from '../components/Set';
import Unit from '../components/Unit';
import Warehouse from '../components/Warehouse';
import Vendor from '../components/Vendor';
import Provider from '../components/Provider';
import Login from '../components/Login';
import Income from "../components/Income";
import Sale from "../components/Sale";


Vue.use(Router);

export default new Router({
    base: '/',
    linkActiveClass: 'active',
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/settings',
            name: 'Settings',
            component: Base,
            redirect: {name: 'Company'},
            meta: {title: "Общие данные", menuTitle: 'Справочники', icon: 'fa-heartbeat', inMenu: true},
            children: [
                {
                    path: 'nds',
                    name: 'Nds',
                    component: Nds,
                    meta: {title: "Общие данные", menuTitle: 'НДС/VAT', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'packing',
                    name: 'Packing',
                    component: Packing,
                    meta: {title: "Общие данные", menuTitle: 'Упаковка', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'set',
                    name: 'Set',
                    component: Set,
                    meta: {title: "Общие данные", menuTitle: 'Комплектация', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'currency',
                    name: 'Currency',
                    component: Currency,
                    meta: {title: "Общие данные", menuTitle: 'Валюта', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'markup',
                    name: 'Markup',
                    component: Markup,
                    meta: {title: "Общие данные", menuTitle: 'Наценка', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'unit',
                    name: 'Unit',
                    component: Unit,
                    meta: {title: "Общие данные", menuTitle: 'Единицы измерения', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'provider',
                    name: 'Provider',
                    component: Provider,
                    meta: {title: "Общие данные", menuTitle: 'Поставщик', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'warehouse',
                    name: 'Warehouse',
                    component: Warehouse,
                    meta: {title: "Общие данные", menuTitle: 'Склады', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'category',
                    name: 'Category',
                    component: Category,
                    meta: {title: "Общие данные", menuTitle: 'Категории', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'vendor',
                    name: 'Vendor',
                    component: Vendor,
                    meta: {title: "Общие данные", menuTitle: 'Производитель', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'company',
                    name: 'Company',
                    component: Company,
                    meta: {title: "Общие данные", menuTitle: 'Компании', icon: 'fa-heartbeat', inMenu: true}
                },

            ]
        },
        {
            path: '/',
            name: 'Base',
            redirect: {name: 'Dashboard'},
            component: Base,
            meta: {title: "Общие данные", menuTitle: 'Для всех', icon: 'fa-heartbeat', inMenu: true},
            children: [
                {path: '404', from: '*', redirect: {name: '404'}},

                {
                    path: 'incomes',
                    name: 'Income',
                    component: Income,
                    meta: {title: "Общие данные", menuTitle: 'Приходная накладная', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'product',
                    name: 'Product',
                    component: Product,
                    meta: {title: "Общие данные", menuTitle: 'Товары', icon: 'fa-heartbeat', inMenu: true}
                },
                {
                    path: 'dashboard',
                    name: 'Dashboard',
                    component: Dashboard,
                    meta: {title: "Общие данные", menuTitle: 'Dashboard', icon: 'fa-heartbeat', inMenu: true}
                },


            ]
        },

        {
            path: '/sale',
            name: 'Sale',
            component: Sale,
            meta: {title: "Общие данные", menuTitle: 'Продажа', icon: 'fa-heartbeat', inMenu: true}
        },

        // { path: '/register', name: 'Register', component: Register, meta: {title: 'Add'} },
        {
            path: '/404',
            name: '404',
            component: NotFound,
            meta: {inMenu: false}
        },
        {
            path: '/settings',
            redirect: {name: 'Company'},
            meta: {inMenu: false}
        },
        {
            path: '/*',
            redirect: {name: '404'},
            meta: {inMenu: false}
        },
    ]
});
