export default {
    data() {
        return {
            paginate: {
                perPage: null
            }
        }
    },
    computed: {
        getModel() {
            return Object.assign(this.model, this.paginate);
        },
        urls() {
            return this.getter('getUrls');
        },
        total() {
            return this.getter('getTotal');
        },
        lastPage() {
            return this.getter('getLastPage');
        }
    },
    methods: {
        changePerPage(data) {
            this.paginate.perPage = data.perPage;
            this.fetch(data);
        },
        fetch(data) {
            return this.$store.dispatch(this.$options.name.toLowerCase() + '/fetch', data);
        },
        getter(item) {
            return this.$store.getters[this.$options.name.toLowerCase() + '/' + item];
        }
    }


}
