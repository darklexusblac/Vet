export default {
    baseUrl: '/api',
    tokenName: 'GtpRfB%Md^v',
    tokenType: 'Bearer',
    contentType: 'application/json',
    requestTimeout: 5000,
    perPage: 20,
    perPages: [20,40,60,80,100],
    bootstrap: [
        'settings/cfTypeOptions'
        /*'warehouse/fetch',
        'market/fetch'*/
    ]
}
