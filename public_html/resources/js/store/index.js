import Vue from 'vue';
import Vuex from 'vuex';
import actions from './actions';
import mutations from './mutations';

import alert from './modules/alert';
import product from './modules/product';
import currency from './modules/currency';
import category from './modules/category';
import company from './modules/company';
import nds from './modules/nds';
import markup from './modules/markup';
import packing from './modules/packing';
import set from './modules/set';
import unit from './modules/unit';
import warehouse from './modules/warehouse';
import vendor from './modules/vendor';
import provider from './modules/provider';
import settings from './modules/settings';
import auth from './modules/auth';
import income from "./modules/income";
import sale from "./modules/sale";

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        loading: false,
        ready: false,
        filters: []
    },
    actions,
    mutations,
    modules: {
        alert,
        product,
        currency,
        category,
        company,
        nds,
        markup,
        packing,
        set,
        unit,
        warehouse,
        vendor,
        provider,
        settings,
        auth,
        income,
        sale
    },
    strict: process.env.NODE_ENV !== 'production'
});
