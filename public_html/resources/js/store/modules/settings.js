import axios from 'axios';

export default {
    namespaced: true,
    state: {
        cfTypes: []
    },
    getters: {
        getCfTypes (state) {
            return state.cfTypes;
        }
    },
    mutations: {
        SET_CF_TYPES (state, items) {
            state.cfTypes = items;
        }
    },
    actions: {
        cfTypeOptions ({commit}) {
            return axios.get('/settings/cf-type').then(response => {
                return commit('SET_CF_TYPES', response.data.items);
            });
        }
    }
}