import axios from 'axios';

export default {
    namespaced: true,
    state: {
        name: null,
        all: [],
        items: [],
        errors: [],
        paginate: {
            urls: [],
            total: 0,
            page: 1,
            lastPage: 0
        },
        products: []
    },
    getters: {
        getName(state) {
            return state.name;
        },
        getItems(state) {
            return state.items;
        },
        getTotal(state) {
            return state.paginate.total;
        },
        getLastPage(state) {
            return state.paginate.lastPage;
        },
        getUrls(state) {
            return state.paginate.urls;
        },
        getErrors(state) {
            return state.errors;
        },
        getAll(state) {
            return state.all;
        },
        getProducts(state) {
            return state.all;
        },
        getChoiced(state) {
            return state.products;
        }
    },
    mutations: {
        ADD_ITTEM(state, item) {
            state.items.push(item);
        },
        FILL(state, response) {
            state.items = response.data;
            state.paginate.total = typeof response.meta === 'undefined' ? response.total : response.meta.total;
            state.paginate.urls = typeof response.meta === 'undefined' ? response.links : response.meta.links;
            state.paginate.lastPage = typeof response.meta === 'undefined' ? response.last_page : response.meta.last_page;
        },
        SET_ERROR(state, error) {
            if (Array.isArray(error)) {
                state.errors = [];
            }
            else {
                state.errors.push(error);
            }
        },
        FILL_ALL(state, items) {
            state.all = items;
        },
        FILL_PRODUCTS(state, item) {
            if (Array.isArray(item)) {
                state.products.concat(item)
            }
            else {
                state.products.push(item);
            }
        },
        CLEAR_PRODUCTS(state, item) {
            if (typeof item === 'undefined') {
                state.products = [];
            }
            else {
                const index = state.products.indexOf(item);
                if (index > -1) {
                    state.products.splice(index, 1);
                }
            }
        }
    },
    actions: {
        fetch({commit, state}, data) {
            store.commit('LOADING', true);
            axios.get('/sale', {
                params: data
            }).then(response => {
                commit('FILL', response.data);
            }).finally(() => {
                store.commit('LOADING', false);
            });
        },

        all({commit}) {
            return axios.get('/sale/all').then(response => {
                commit('FILL_ALL', response.data.items);
            });
        },

        save({commit, state, dispatch}, {data, config}) {
            store.commit('LOADING', true);

            data = Object.assign({}, state.paginate, data);
            let request_config = {};
            let formData = data;
            if (config.needFile || false) {
                request_config = {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    processData: false,
                    contentType: false,
                    mimeType: "multipart/form-data"
                };

                formData = new FormData();
                for (let field in data) {
                    if (!data.hasOwnProperty(field) || data[field] === null) { continue; }
                    formData.append(field, data[field]);
                }
            }

            return axios.post('/sale/save', formData, request_config)
                .then(response => {
                    store.dispatch('alert/success', response.data.message);
                    commit('FILL', response.data.items);

                    return Promise.resolve(response);
                }).finally(() => {
                    store.commit('LOADING', false);
                })
                .catch(error => {
                    store.dispatch('handle', error, {root: true});
                    return Promise.reject(error);
                })
        },
        delete({commit, state}, data) {
            store.commit('LOADING', true);
            axios.post('/sale/delete', Object.assign({}, data, state.paginate))
                .then(response => {
                    store.dispatch('alert/success', response.data.message);
                    commit('FILL', response.data.items);

                }).finally(() => {
                store.commit('LOADING', false);
            })
                .catch(error => {
                    console.error(error);
                })
        }
    }
};
