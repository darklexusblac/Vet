import axios from 'axios';
import {toJSON} from "lodash/seq";

export default {
    namespaced: true,
    state: {
        name: null,
        all: [],
        items: [],
        products: [],
        errors: [],
        paginate: {
            urls: [],
            total: 0,
            page: 1,
            lastPage: 0
        },
        choiced: []
    },
    getters: {
        getName(state) {
            return state.name;
        },
        getItems(state) {
            return state.items;
        },
        getTotal(state) {
            return state.paginate.total;
        },
        getLastPage(state) {
            return state.paginate.lastPage;
        },
        getUrls(state) {
            return state.paginate.urls;
        },
        getErrors(state) {
            return state.errors;
        },
        getAll(state) {
            return state.all;
        },
        getProducts(state) {
            return state.products;
        },
        getChoiced(state) {
            return state.choiced;
        }
    },
    mutations: {
        ADD_ITTEM(state, item) {
            state.items.push(item);
        },
        FILL(state, response) {
            state.items = response.data;
            state.paginate.total = typeof response.meta === 'undefined' ? response.total : response.meta.total;
            state.paginate.urls = typeof response.meta === 'undefined' ? response.links : response.meta.links;
            state.paginate.lastPage = typeof response.meta === 'undefined' ? response.last_page : response.meta.last_page;
        },
        SET_ERROR(state, error) {
            if (Array.isArray(error)) {
                state.errors = [];
            }
            else {
                state.errors.push(error);
            }
        },
        FILL_ALL(state, items) {
            state.all = items;
        },
        FILL_PARTNER_CARS(state, items) {
            state.partner_cars = items;
        },
        FILL_PRODUCTS(state, items) {
            state.products = items;
            state.paginate.total = typeof response.meta === 'undefined' ? response.total : response.meta.total;
            state.paginate.urls = typeof response.meta === 'undefined' ? response.links : response.meta.links;
            state.paginate.lastPage = typeof response.meta === 'undefined' ? response.last_page : response.meta.last_page;
        },
        SET_CHOICED(state, item) {
            if (Array.isArray(item)) {
                state.choiced.concat(item)
            }
            else {
                state.choiced.push(item);
            }
        },
        CLEAR_CHOICE(state, item) {
            if (typeof item === 'undefined') {
                state.choiced = [];
            }
            else {
                const index = state.choiced.indexOf(item);
                if (index > -1) {
                    state.choiced.splice(index, 1);
                }
            }
        }
    },
    actions: {
        fetch({commit, state}, data) {
            store.commit('LOADING', true);
            axios.get('/product?filters=' + JSON.stringify(store.state.filters), {
                params: data
            }).then(response => {
                commit('FILL', response.data);
            }).finally(() => {
                store.commit('LOADING', false);
            });
        },
        all({commit}) {
            return axios.get('/product/all').then(response => {
                commit('FILL_ALL', response.data.items);
            });
        },
        save({commit, state, dispatch}, {data, config}) {
            store.commit('LOADING', true);

            data = Object.assign({}, state.paginate, data);
            let request_config = {};
            let formData = data;

            if (config.needFile || false) {
                request_config = {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    processData: false,
                    contentType: false,
                    mimeType: "multipart/form-data"
                };

                formData = new FormData();
                for (let field in data) {
                    if (!data.hasOwnProperty(field) || data[field] === null) { continue; }

                    if (Array.isArray(data[field])) {
                        data[field] = JSON.stringify(data[field]);
                    }
                    formData.append(field, data[field]);
                }
            }

            return axios.post('/product/save', formData, request_config)
                .then(response => {
                    store.dispatch('alert/success', response.data.message);
                    commit('FILL', response.data.items);

                    return Promise.resolve(response);
                })
                .finally(() => {
                    store.commit('LOADING', false);
                })
                .catch(error => {
                    store.dispatch('handle', error, {root: true});
                    return Promise.reject(error);
                })
        },
        delete({commit, state}, data) {
            store.commit('LOADING', true);
            axios.post('/product/delete', Object.assign({}, data, state.paginate))
                .then(response => {
                    store.dispatch('alert/success', response.data.message);
                    commit('FILL', response.data.items);

                }).finally(() => {
                store.commit('LOADING', false);
            })
                .catch(error => {
                    console.error(error);
                })
        },
        search({commit}, data) {
            return axios.get('/product/search', {params: data}).then(response => {
                return response.data.items;
            })
                .catch(error => {console.error(error);});
        }
    }
};
