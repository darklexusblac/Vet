export default {
    LOADING(state, loading) {
        state.loading = Boolean(loading)
    },
    READY(state) {
        state.ready = true
    },
    FILTER(state, data) {
        if (state.filters.length === 0) {
            state.filters.push(data)
        }
        else {
            state.filters = state.filters.map(item => {
                let itemKeys = Object.keys(item);
                for (let element in data) {
                    if (itemKeys.indexOf(element) > -1) {
                        let newItem = {};
                        newItem[element] = data[element];
                        return newItem;
                    }
                }
                return item;
            });
        }
    },
    CLEAR_FILTER(state) {
        state.filters = []
    }
}
