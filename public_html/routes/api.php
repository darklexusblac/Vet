<?php

use App\Http\Controllers\Api\SpaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\CurrencyController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CompanyController;
use App\Http\Controllers\Api\NdsController;
use App\Http\Controllers\Api\MarkupController;
use App\Http\Controllers\Api\PackingController;
use App\Http\Controllers\Api\SetController;
use App\Http\Controllers\Api\UnitController;
use App\Http\Controllers\Api\WarehouseController;
use App\Http\Controllers\Api\VendorController;
use App\Http\Controllers\Api\ProviderController;
use App\Http\Controllers\Api\CustomFieldController;
use App\Http\Controllers\Api\IncomeController;
use App\Http\Controllers\Api\SaleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
const CF_TYPES = [
    ['id' => 'text', 'name' => 'Текст'],
    ['id' => 'number', 'name' => 'Цифры']
];

//Route::post('/test', function () {
//    ProductController::class->search(1)->get();
//});

Route::post('/token', [SpaController::class, 'token']);

Route::group([
    'middleware' => 'auth:sanctum'
], function () {
    Route::get('/user', [SpaController::class, 'user']);
    Route::post('/rm_token', [SpaController::class, 'rm_token']);

    Route::group(['prefix' => 'settings'], function () {
        Route::get('/cf-type', function () {
            return ['items' => CF_TYPES];
        });
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/', [ProductController::class, 'index']);
        Route::post('/save', [ProductController::class, 'save']);
        Route::post('/delete', [ProductController::class, 'delete']);
        Route::get('/all', [ProductController::class, 'all']);
        Route::get('/search', [ProductController::class, 'search']);
        Route::get('/get-product-warehouse', [ProductController::class, 'getProductWarehouse']);
    });

    Route::group(['prefix' => 'currency'], function () {
        Route::get('/', [CurrencyController::class, 'index']);
        Route::post('/save', [CurrencyController::class, 'save']);
        Route::post('/delete', [CurrencyController::class, 'delete']);
        Route::get('/all', [CurrencyController::class, 'all']);
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('/', [CategoryController::class, 'index']);
        Route::post('/save', [CategoryController::class, 'save']);
        Route::post('/delete', [CategoryController::class, 'delete']);
        Route::get('/all', [CategoryController::class, 'all']);
        Route::get('/children/{root}', [CategoryController::class, 'getChildren']);
        Route::get('/all4product', [CategoryController::class, 'allWithoutTree']);
    });

    Route::group(['prefix' => 'company'], function () {
        Route::get('/', [CompanyController::class, 'index']);
        Route::post('/save', [CompanyController::class, 'save']);
        Route::post('/delete', [CompanyController::class, 'delete']);
        Route::get('/all', [CompanyController::class, 'all']);
    });

    Route::group(['prefix' => 'nds'], function () {
        Route::get('/', [NdsController::class, 'index']);
        Route::post('/save', [NdsController::class, 'save']);
        Route::post('/delete', [NdsController::class, 'delete']);
        Route::get('/all', [NdsController::class, 'all']);
    });

    Route::group(['prefix' => 'markup'], function () {
        Route::get('/', [MarkupController::class, 'index']);
        Route::post('/save', [MarkupController::class, 'save']);
        Route::post('/delete', [MarkupController::class, 'delete']);
        Route::get('/all', [MarkupController::class, 'all']);
    });

    Route::group(['prefix' => 'packing'], function () {
        Route::get('/', [PackingController::class, 'index']);
        Route::post('/save', [PackingController::class, 'save']);
        Route::post('/delete', [PackingController::class, 'delete']);
        Route::get('/all', [PackingController::class, 'all']);
    });

    Route::group(['prefix' => 'set'], function () {
        Route::get('/', [SetController::class, 'index']);
        Route::post('/save', [SetController::class, 'save']);
        Route::post('/delete', [SetController::class, 'delete']);
        Route::get('/all', [SetController::class, 'all']);
    });

    Route::group(['prefix' => 'unit'], function () {
        Route::get('/', [UnitController::class, 'index']);
        Route::post('/save', [UnitController::class, 'save']);
        Route::post('/delete', [UnitController::class, 'delete']);
        Route::get('/all', [UnitController::class, 'all']);
    });

    Route::group(['prefix' => 'warehouse'], function () {
        Route::get('/', [WarehouseController::class, 'index']);
        Route::post('/save', [WarehouseController::class, 'save']);
        Route::post('/delete', [WarehouseController::class, 'delete']);
        Route::get('/all', [WarehouseController::class, 'all']);
    });

    Route::group(['prefix' => 'vendor'], function () {
        Route::get('/', [VendorController::class, 'index']);
        Route::post('/save', [VendorController::class, 'save']);
        Route::post('/delete', [VendorController::class, 'delete']);
        Route::get('/all', [VendorController::class, 'all']);
    });

    Route::group(['prefix' => 'provider'], function () {
        Route::get('/', [ProviderController::class, 'index']);
        Route::post('/save', [ProviderController::class, 'save']);
        Route::post('/delete', [ProviderController::class, 'delete']);
        Route::get('/all', [ProviderController::class, 'all']);
    });

    Route::group(['prefix' => 'income'], function () {
        Route::get('/', [IncomeController::class, 'index']);
        Route::post('/save', [IncomeController::class, 'save']);
        Route::post('/delete', [IncomeController::class, 'delete']);
        Route::get('/all', [IncomeController::class, 'all']);
    });

    Route::group(['prefix' => 'sale'], function () {
        Route::get('/', [SaleController::class, 'index']);
        Route::post('/save', [SaleController::class, 'save']);
        Route::post('/delete', [SaleController::class, 'delete']);
        Route::get('/all', [SaleController::class, 'all']);
    });
});

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});




